
var Welpworld = function() {};

Welpworld.Boot = function() {};

Welpworld.Boot.prototype = {
  preload: function() {

    jogo.carregarImagem('logo', 'assets/images/logo.png');
    jogo.carregarImagem('preloadbar', 'assets/images/preloader-bar.png');
  
},
  create: function() {
    jogo.corFundo('#FFF');
    
    //  Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
    jogo.numeroToques(1);

    if (jogo.paraDispositivoMovel()) {
      jogo.definirDimensoesMovel(568,600,2048,1536,true);
    }

    jogo.activarEstado('Preloader');
  }
};

Welpworld.Preload = function() {
  this.pronto = jogo.falso;
  
};

Welpworld.Preload.prototype = {
  preload: function() {

    this.splash =jogo.utilizarImagem(jogo.centroX(), jogo.centroY(), 0.5, 'logo');
  
    this.barra = jogo.utilizarImagem(jogo.centroX(), jogo.centroY() + 150, 0.5, 'preloadbar');
    
    jogo.definirBarraCarregamento(this.barra);
 
    jogo.carregarImagem('fundo', 'assets/images/mapa.png');
    jogo.carregarImagem('c1', 'assets/images/c1.png');
    jogo.carregarImagem('c2', 'assets/images/c2.png');
    jogo.carregarImagem('c3', 'assets/images/c3.png');
    jogo.carregarImagem('c4', 'assets/images/c4.png');
    jogo.carregarImagem('c5', 'assets/images/c5.png');
    jogo.carregarImagem('c6', 'assets/images/c6.png');
    jogo.carregarImagem('c7', 'assets/images/c7.png');
    jogo.carregarImagem('c8', 'assets/images/c8.png');
    jogo.carregarImagem('c9', 'assets/images/c9.png');
    jogo.carregarImagem('cbau', 'assets/images/cbau.png');
    
    jogo.carregarImagem('v1', 'assets/images/v1.png');
    jogo.carregarImagem('v2', 'assets/images/v2.png');
    jogo.carregarImagem('v3', 'assets/images/v3.png');
    jogo.carregarImagem('v4', 'assets/images/v4.png');
    jogo.carregarImagem('v5', 'assets/images/v5.png');
    jogo.carregarImagem('v6', 'assets/images/v6.png');
    jogo.carregarImagem('v9', 'assets/images/v9.png');
    jogo.carregarImagem('v7', 'assets/images/v7.png');
    jogo.carregarImagem('v8', 'assets/images/v8.png');
    jogo.carregarImagem('vbau', 'assets/images/vbau.png');
  
    
   //This needs something to load or won't be called'
    jogo.carregamentoCompleto(this.carregamentoCompleto,this);
  },
  
  update: function() {
   if( this.pronto === jogo.verdade ){
   jogo.activarEstado('Game');
    }
  },
 
  carregamentoCompleto: function() {
    this.pronto = jogo.verdade;
  },

 
};


Welpworld.Game = function() {
 
 this.clickedImg=null;

 this.positions=
 [
   {x:285,y:25},
   {x:228,y:79},
   {x:212,y:146},
   {x:257,y:317},
   {x:391,y:363},
   {x:470,y:285},
   {x:468,y:235},
   {x:477,y:189},
   {x:608,y:172},
   {x:590,y:90},
   
 ]

};

Welpworld.Game.prototype = {
  create: function() {
      
      this.exercise=document.getElementById("next_exercise");
      
    this.fundo = jogo.utilizarImagem(15,0 ,0,'fundo');
    jogo.definirEscalaObjecto(this.fundo,0.91);
    var imagem=null;

    imagem = jogo.utilizarImagem(285,25,0,'c1');
    if(this.exercise.value > 0){
        imagem.inputEnabled = true;
        imagem.events.onInputDown.add(this.clickedOne, this); 
        
    }

    imagem = jogo.utilizarImagem(228,79,0,'c2');
  if(this.exercise.value > 1){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedTwo, this);
}
    imagem = jogo.utilizarImagem(212,146,0,'c3');
 if(this.exercise.value > 2){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedThree, this);
}
    imagem = jogo.utilizarImagem(257,317,0,'c4');
 if(this.exercise.value > 3){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedFour, this);
}
    imagem = jogo.utilizarImagem(391,363,0,'c5');
 if(this.exercise.value > 4){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedFive, this);
}
    imagem = jogo.utilizarImagem(470,285,0,'c6');
 if(this.exercise.value > 5){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedSix, this);
}
    imagem = jogo.utilizarImagem(468,235,0,'c7');
 if(this.exercise.value > 6){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedSeven, this);
}
    imagem = jogo.utilizarImagem(477,189,0,'c8');
 if(this.exercise.value > 7){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedEight, this);
 }
    imagem = jogo.utilizarImagem(608,172,0,'c9');
  if(this.exercise.value > 8){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedNine, this);
}
    imagem = jogo.utilizarImagem(590,90,0,'cbau');
 if(this.exercise.value > 9){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedChest, this);
 }

switch (parseInt(this.exercise.value)) {
     case 1:
         this.clickedOne();
         break;
     case 2:
         this.clickedTwo();
         break;
     case 3:
         this.clickedThree();
         break;
     case 4:
         this.clickedFour();
         break;
     case 5:
         this.clickedFive();
         break;
     case 6:
         this.clickedSix();
         break;
     case 7:
         this.clickedSeven();
         break;
     case 8:
         this.clickedEight();
         break;
     case 9:
         this.clickedNine();
         break;
     case 10:
         this.clickedChest();
         break;
     default:
         alert(this.exercise.value);
 }

},
  clickedOne: function() {  
    if (this.clickedImg!=null)
        this.clickedImg.destroy();
    
    var pos=this.positions[0];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v1');
    this.exercise.value = 1;
  },
   clickedTwo: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[1];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v2');
this.exercise.value = 2;
  },
   clickedThree: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[2];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v3');
this.exercise.value = 3;
  },
   clickedFour: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[3];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v4');
this.exercise.value = 4;
  },
   clickedFive: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[4];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v5');
    this.exercise.value = 5;
  },
   clickedSix: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[5];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v6');
this.exercise.value = 6;
  },
   clickedSeven: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[6];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v7');
this.exercise.value = 7;
  },
   clickedEight: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[7];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v8');
this.exercise.value = 8;
  },
   clickedNine: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[8];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v9');
this.exercise.value = 9;
  },
   clickedChest: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[9];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'vbau');
this.exercise.value = 10;
  },
  
};

var jogo = new Motor();

jogo.tela(832.5 ,416.27,"game");

jogo.adicionarEstado('Boot', Welpworld.Boot);
jogo.adicionarEstado('Preloader', Welpworld.Preload);
jogo.adicionarEstado('Game', Welpworld.Game);

jogo.activarEstado('Boot');