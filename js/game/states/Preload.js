Welpworld.Preload = function() {
  this.pronto = jogo.falso;
  
};

Welpworld.Preload.prototype = {
  preload: function() {

    this.splash =jogo.utilizarImagem(jogo.centroX(), jogo.centroY(), 0.5, 'logo');
  
    this.barra = jogo.utilizarImagem(jogo.centroX(), jogo.centroY() + 150, 0.5, 'preloadbar');
    
    jogo.definirBarraCarregamento(this.barra);
 
    jogo.carregarImagem('fundo', 'assets/images/mapa.png');
    jogo.carregarImagem('c1', 'assets/images/c1.png');
    jogo.carregarImagem('c2', 'assets/images/c2.png');
    jogo.carregarImagem('c3', 'assets/images/c3.png');
    jogo.carregarImagem('c4', 'assets/images/c4.png');
    jogo.carregarImagem('c5', 'assets/images/c5.png');
    jogo.carregarImagem('c6', 'assets/images/c6.png');
    jogo.carregarImagem('c7', 'assets/images/c7.png');
    jogo.carregarImagem('c8', 'assets/images/c8.png');
    jogo.carregarImagem('c9', 'assets/images/c9.png');
    jogo.carregarImagem('cbau', 'assets/images/cbau.png');
    
    jogo.carregarImagem('v1', 'assets/images/v1.png');
    jogo.carregarImagem('v2', 'assets/images/v2.png');
    jogo.carregarImagem('v3', 'assets/images/v3.png');
    jogo.carregarImagem('v4', 'assets/images/v4.png');
    jogo.carregarImagem('v5', 'assets/images/v5.png');
    jogo.carregarImagem('v6', 'assets/images/v6.png');
    jogo.carregarImagem('v9', 'assets/images/v9.png');
    jogo.carregarImagem('v7', 'assets/images/v7.png');
    jogo.carregarImagem('v8', 'assets/images/v8.png');
    jogo.carregarImagem('vbau', 'assets/images/vbau.png');
  
    
   //This needs something to load or won't be called'
    jogo.carregamentoCompleto(this.carregamentoCompleto,this);
  },
  
  update: function() {
   if( this.pronto === jogo.verdade ){
   jogo.activarEstado('Game');
    }
  },
 
  carregamentoCompleto: function() {
    this.pronto = jogo.verdade;
  },

 
};