Welpworld.Game = function() {
 
 this.clickedImg=null;

 this.positions=
 [
   {x:285,y:25},
   {x:228,y:79},
   {x:212,y:146},
   {x:257,y:317},
   {x:391,y:363},
   {x:470,y:285},
   {x:468,y:235},
   {x:477,y:189},
   {x:608,y:172},
   {x:590,y:90},
   
 ]

};

Welpworld.Game.prototype = {
  create: function() {
      
      this.exercise=document.getElementById("execise");
      
    this.fundo = jogo.utilizarImagem(15,0 ,0,'fundo');
    jogo.definirEscalaObjecto(this.fundo,0.91);
    var imagem=null;

    imagem = jogo.utilizarImagem(285,25,0,'c1');
    if(this.exercise.value > 0){
        imagem.inputEnabled = true;
        imagem.events.onInputDown.add(this.clickedOne, this); 
        
    }

    imagem = jogo.utilizarImagem(228,79,0,'c2');
  if(this.exercise.value > 1){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedTwo, this);
}
    imagem = jogo.utilizarImagem(212,146,0,'c3');
 if(this.exercise.value > 2){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedThree, this);
}
    imagem = jogo.utilizarImagem(257,317,0,'c4');
 if(this.exercise.value > 3){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedFour, this);
}
    imagem = jogo.utilizarImagem(391,363,0,'c5');
 if(this.exercise.value > 4){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedFive, this);
}
    imagem = jogo.utilizarImagem(470,285,0,'c6');
 if(this.exercise.value > 5){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedSix, this);
}
    imagem = jogo.utilizarImagem(468,235,0,'c7');
 if(this.exercise.value > 6){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedSeven, this);
}
    imagem = jogo.utilizarImagem(477,189,0,'c8');
 if(this.exercise.value > 7){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedEight, this);
 }
    imagem = jogo.utilizarImagem(608,172,0,'c9');
  if(this.exercise.value > 8){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedNine, this);
}
    imagem = jogo.utilizarImagem(590,90,0,'cbau');
 if(this.exercise.value > 9){
    imagem.inputEnabled = true;
    imagem.events.onInputDown.add(this.clickedChest, this);
 }
},
  clickedOne: function() {  
    if (this.clickedImg!=null)
        this.clickedImg.destroy();
    
    var pos=this.positions[0];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v1');
    this.exercise.value = 1;
  },
   clickedTwo: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[1];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v2');
this.exercise.value = 2;
  },
   clickedThree: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[2];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v3');
this.exercise.value = 3;
  },
   clickedFour: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[3];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v4');
this.exercise.value = 4;
  },
   clickedFive: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[4];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v5');
    this.exercise.value = 5;
  },
   clickedSix: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[5];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v6');
this.exercise.value = 6;
  },
   clickedSeven: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[6];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v7');
this.exercise.value = 7;
  },
   clickedEight: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[7];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v8');
this.exercise.value = 8;
  },
   clickedNine: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[8];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'v9');
this.exercise.value = 9;
  },
   clickedChest: function() {  
      if (this.clickedImg!=null)
        this.clickedImg.destroy();
        
    var pos=this.positions[9];
    this.clickedImg = jogo.utilizarImagem(pos.x,pos.y,0,'vbau');
this.exercise.value = 10;
  },
  
};